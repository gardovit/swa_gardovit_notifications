<?php

declare(strict_types=1);

namespace App;

use LogstashLogger;
use Nette\Bootstrap\Configurator;


class Bootstrap
{
	public static function boot(): Configurator
	{
		$configurator = new Configurator;
		$appDir = dirname(__DIR__);

		//$configurator->setDebugMode(['secret@23.75.345.200', 'ITboMjSPfM6McY4oLWI8uKthXMdmZ6nx']); // enable for your remote IP
		$configurator->setDebugMode(true);
		$configurator->enableTracy($appDir . '/log');

		$configurator->setTimeZone('Europe/Prague');
		$configurator->setTempDirectory($appDir . '/temp');

		$configurator->createRobotLoader()
			->addDirectory(__DIR__)
			->register();

		$configurator->addConfig($appDir . '/config/config.neon');

		$GLOBALS['robotLoader'] = $configurator->createRobotLoader()
			->addDirectory(__DIR__)
			->addDirectory(__DIR__.'/../libs')
			->register();

		return $configurator;
	}
}
