<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;

final class HomepagePresenter extends \BasePresenter
{
	/** @inject @var \Nette\Database\Connection */
	public Nette\Database\Connection $db;

	public function formSucceeded(Form $form, $data){
	}

	public function renderDefault(): void
	{
	}

	public function actionDefault()
	{

	}

	public function actionHome(){
	}

	public function handleClick(int $x, int $y): void
	{
	}

}
