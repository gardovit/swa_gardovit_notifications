<?php


abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	/** @inject @var \Nette\Database\Connection */
	public Nette\Database\Connection $db;

	/** @inject @var \Nette\Routing\Router */
	public \Nette\Routing\Router $router;

	/** @inject @var \Nette\Security\User */
	public \Nette\Security\User $user;

	/** @persistent */
	public $lang;

	/** @inject $@ar App\Translate\GettextTranslator */
	public GettextTranslator $translator;


	public function startup(){

		parent::startup();

		/*
		 * Language detection
		 */

		$sectionLang = $this->session->getSection('language');
		//$this->lang = $sectionLang->get('lang');

		if(!$this->lang){
			$this->lang = $this->getHttpRequest()->detectLanguage(['en', 'cs', 'de', 'pl']);
		}

		if(!$this->lang){
			$this->lang = 'en';
		}

		$sectionLang->set('lang', $this->lang);

		$this->translator->setLang($this->lang);

		$this->template->lang = $this->lang;

		switch($this->lang){
			case 'en':
				$this->template->languages = [
					'en' => $this->link('this', ['lang' => 'en']),
					'cs' => $this->link('this', ['lang' => 'cs']),
					'de' => $this->link('this', ['lang' => 'de']),
					'pl' => $this->link('this', ['lang' => 'pl']),
				];
				break;
			case 'de':
				$this->template->languages = [
					'de' => $this->link('this', ['lang' => 'de']),
					'cs' => $this->link('this', ['lang' => 'cs']),
					'en' => $this->link('this', ['lang' => 'en']),
					'pl' => $this->link('this', ['lang' => 'pl']),
				];
				break;
			case 'pl':
				$this->template->languages = [
					'pl' => $this->link('this', ['lang' => 'pl']),
					'cs' => $this->link('this', ['lang' => 'cs']),
					'en' => $this->link('this', ['lang' => 'en']),
					'de' => $this->link('this', ['lang' => 'de']),
				];
				break;
			case 'cs':
			default:
				$this->template->languages = [
					'cs' => $this->link('this', ['lang' => 'cs']),
					'en' => $this->link('this', ['lang' => 'en']),
					'de' => $this->link('this', ['lang' => 'de']),
					'pl' => $this->link('this', ['lang' => 'pl']),
				];
				break;
		}

	}

	public function createTemplate($class = NULL): Nette\Application\UI\ITemplate
	{
		parent::createTemplate($class);

		$template = parent::createTemplate($class);
		$template->setTranslator($this->translator);

		$template->lang = $this->lang;

		return $template;
	}


	public function formatLayoutTemplateFiles(): array
	{
		$name = $this->getName();
		$presenter = substr($name, strrpos(':'.$name, ':'));
		$layout = $this->layout ? $this->layout : 'layout';

		/* Adresář, ve kterém je aktuální presenter. */
		$dir = dirname($this->getReflection()->getFileName());
		$dir = is_dir("$dir/templates") ? $dir : dirname($dir);
		$list =
		[
			"$dir/templates/@$layout.latte",
		];

		do
		{
			$dir = dirname($dir);
			$list[] = "$dir/Presenters/templates/@$layout.latte";
		}
		while($dir && ($name = substr($name, 0, strrpos($name, ':'))));

		return $list;
	}

	public function formatTemplateFiles(): array
	{
		$name = $this->getName();
		$presenter = substr($name, strrpos(':'.$name, ':'));

		/* Adresář, ve kterém je aktuální presenter. */
		$dir = dirname($this->getReflection()->getFileName());
		$dir = is_dir("$dir/templates") ? $dir : dirname($dir);

		return
		[
			"$dir/templates/$this->view.latte",
		];
	}

}
