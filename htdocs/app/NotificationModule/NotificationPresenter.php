<?php

namespace App\Presenters;

use Exception;
use GuzzleHttp\Client;
use Nette\Application\Responses\JsonResponse;
use Tracy\Debugger;
use Tracy\ILogger;

final class NotificationPresenter extends \BasePresenter
{
	public function __construct(public \Nette\Database\Connection $db)
	{
	}

	/**
	 * @openapi
	 * /migrate:
	 *   post:
	 *     summary: Creates tables of notification database
	 *     description: Notification service will not work with uninitialized database.
	 *      Its required to run this before any other endpoint is accessed
	 *     tags:
	 *       - Notification
	 *     responses:
	 *       200:
	 *         description: Database successfully initialized
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: string
	 *               example: "Database initialized"
	 *       500:
	 *         description: Error with database initialization
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 error:
	 *                   type: string
	 *                   example: "Could not connect to database"
	 *       405:
	 *         description: Used method not allowed
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: string
	 *               example: "Method Not Allowed"
	 */
	public function actionMigrate(){
		if($this->request->isMethod('POST') == false) {
			$this->respondJSON('Method Not Allowed', 405);
		}

		try{
			$this->db->query("
			CREATE TABLE IF NOT EXISTS notifications (
				id SERIAL ,
				user_id INT NOT NULL,
				user_email TEXT NOT NULL,
				type TEXT NOT NULL,
				data TEXT,
				send_time TIMESTAMP NOT NULL,

				PRIMARY KEY(id)
			);
			");
		}
		catch(\Exception $e){
			Debugger::log(['error' => 'Problem with database migration'], ILogger::EXCEPTION);
			$this->respondJSON(['error' => $e->getMessage()], 500);
		}

		Debugger::log(['message' => 'Database initialized'], ILogger::INFO);
		$this->respondJSON("Database initialized", 200);
	}


	/**
	 * @openapi
	 * /health:
	 *   get:
	 *     summary: Health check endpoint
	 *     description: Response if service is running well
	 *     tags:
	 *       - Notification
	 *     responses:
	 *       200:
	 *         description: Service is healthy
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 status:
	 *                   type: string
	 *                   example: "OK"
	 *       405:
	 *         description: Used method not allowed
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: string
	 *               example: "Method Not Allowed"
	 */
	public function actionHealth(){
		if($this->request->isMethod('GET') == false) {
			$this->respondJSON('Method Not Allowed', 405);
		}

		$this->respondJSON(['status' => 'OK'], 200);
	}


	/**
	 * @openapi
	 * /registered/{userId}:
	 *   post:
	 *     summary: Notify user that registration was successful
	 *     description: Notify user that registration was successful
	 *     tags:
	 *       - Notification
	 *     parameters:
	 *       - name: userId
	 *         in: path
	 *         required: true
	 *         schema:
	 *           type: integer
	 *           example: 1
	 *           description: ID of newly registered user
	 *     responses:
	 *       200:
	 *         description: User notified successfully
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 message:
	 *                   type: string
	 *                   example: "Notification send"
	 *       405:
	 *         description: Used method not allowed
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: string
	 *               example: "Method Not Allowed"
	 */
	public function actionRegistered($userId){

		if($this->request->isMethod('POST') == false) {
			$this->respondJSON('Method Not Allowed', 405);
		}

		$user_id = $userId;

		if($user_id == null){
			$this->respondJSON(['error' => 'user ID not specified'], 400);
		}


		//Send notification
		$this->sendNotification($user_id, $user_id, 'registration', ['message' => 'You were registered now.']);

		$this->respondJSON(['message' => 'Notification send'], 200);
	}




	/**
	 * @openapi
	 * /notify/{userId}:
	 *   post:
	 *     summary: Notify
	 *     description: Send notification to each user that voted yes for a sessionTime that is about to begin soon
	 *     tags:
	 *       - Notification
	 *     parameters:
	 *       - in: path
	 *         name: userId
	 *         required: true
	 *         schema:
	 *           type: integer
	 *           example: 2
	 *         description: ID of user that should recieve notification
	 *       - in: query
	 *         name: data
	 *         required: true
	 *         schema:
	 *           type: object
	 *           properties:
	 *              type:
	 *                type: string
	 *                example: "popup"
	 *              data:
	 *                type: object
	 *                example: {message: "REEE"}
	 *     responses:
	 *       200:
	 *         description: Everything went smoothly
	 *       500:
	 *         description: WServer side error
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 error:
	 *                   type: string
	 *                   example: "Server-side error"
	 *       410:
	 *         description: Authentification is not registered in service register
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 error:
	 *                   type: string
	 *                   example: "Authentification service not found"
	 *       405:
	 *         description: Used method not allowed
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: string
	 *               example: "Method Not Allowed"
	 *
	 *       404:
	 *         description: User's email not found
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 error:
	 *                   type: string
	 *                   example: "User's email not found"
	 */
	public function actionNotify($userId){
		if($this->request->isMethod('POST') == false) {
			$this->respondJSON('Method Not Allowed', 405);
		}

		$client = new Client;

		//Get authentication service URL
		try{
			$response = $client->request('GET', getenv('SERVICE_REGISTER_URL').'/services?type=authentication', []);

			$body = $response->getBody()->getContents();
			$authURL = json_decode($body, true);
		}
		catch(Exception $e){
			Debugger::log(['message' => 'Cannot get auth service URL'], ILogger::ERROR);
			$this->respondJSON(['error' => 'Cannot retrieve authentification URL'], 500);
		}

		if($authURL == null || $authURL == ""){
			//Authentification not found
			Debugger::log(['message' => 'Cannot find authentificatio nservice'], ILogger::ERROR);
			$this->respondJSON(['error' => 'Authentification service not found'], 410);
		}


		//Get user information
		try{
			$response = $client->request('GET', $authURL.'/users/'.$userId, []);

			$body = $response->getBody()->getContents();
			$user = json_decode($body, true);
		}
		catch(Exception $e){
			bdump($e, 'exception');

			Debugger::log(['error' => 'Cannot retrieve user from authentification'], ILogger::ERROR);
			$this->respondJSON(['error' => 'Cannot retrieve user from authentification'], 500);
		}


		if($user == null || isset($user['username']) == false || $user['username'] == null){
			//Email not retrieved
			Debugger::log(['message' => "username of user $userId cannot be found"], ILogger::INFO);
			$this->respondJSON(['error' => "User's username not found"], 404);
		}



		//Get input data
		$rawBody = file_get_contents('php://input');
		$data = json_decode($rawBody, true);

		$this->sendNotification($userId, $user['username'], $data['type'] ?? "", $data['data'] ?? null);


		$this->respondJSON(['message' => 'Notification send'], 200);
	}





	/**
	 * @openapi
	 * /last-notification:
	 *   get:
	 *     summary: Get info about last notification
	 *     description: Get info about last notification
	 *     tags:
	 *       - Notification
	 *     responses:
	 *       200:
	 *         description: Last notification retrieved
	 *       404:
	 *         description: No notification was send yet
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *       500:
	 *         description: Internal error
	 */
	public function actionLastNotification(){

		if($this->request->isMethod('GET') == false) {
			$this->respondJSON('Method Not Allowed', 405);
		}

		$notification = $this->db->fetch("SELECT user_id, user_email, type, data FROM notifications ORDER BY send_time DESC",);

		if(!$notification){
			$this->respondJSON(null, 404);
			return;
		}

		$this->respondJSON($notification, 200);
	}














	public function sendNotification($user_id, $user_email, $type, $data){
		Debugger::log([
			'message' => 'sending notification',
			'user_id' => $user_id,
			'user_email' => $user_email,
			'type' => $type,
			'data' => $data
		], ILogger::INFO);
		$this->db->query("
			INSERT INTO notifications(user_id, user_email, type, data, send_time) VALUES(?, ?, ?, ?, NOW())
		", $user_id, $user_email, $type, json_encode($data));
	}


	public function respondJSON($data, $code = 200): void
	{
		$jsonResponse = new JsonResponse($data);
		$this->getHttpResponse()->setCode($code);
		$this->sendResponse($jsonResponse);
	}
}
