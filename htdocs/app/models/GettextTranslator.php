<?php

class GettextTranslator implements Nette\Localization\ITranslator
{
	public static function createTranslator($dir)
	{
		return new static($dir);
	}

	public function __construct(public $dir) { }

	public function setLang($lang)
	{
	}

	public function translate($message, ...$parameters): string
	{
		if(!$this->dir) return $message;
		return count($parameters) ? ngettext($message, ...$parameters) : gettext($message);
	}
}
