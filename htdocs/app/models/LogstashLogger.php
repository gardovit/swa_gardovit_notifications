<?php

use GuzzleHttp\Client;
use Tracy\ILogger;


class LogstashLogger implements ILogger
{
	public function log($value, $priority = ILogger::INFO)
	{
		//Elastic log
		$logstashHost = getenv('LOGSTASH_URL') ?? 'http://logstash:5000';
		$client = new Client();
		$logData = [
			'timestamp' => date('Y-m-d H:i:s'),
			'message' => 'Tracy log',
			'level' => 'log',
			'event_type' => $priority,
			'metadata' => $value
		];

		try {
			$response = $client->post($logstashHost, [
				'json' => $logData
			]);
		} catch (Exception $e) {
			//Log error locally
			$this->file_log([
				'message' => 'Cannot send elastic log',
				'error_message' => $e->getMessage()
			], ILogger::ERROR);
		}

		//Log locally
		$this->file_log($value, $priority);
	}


	public function file_log($value, $priority = ILogger::INFO){
		$log_file = getenv('LOG_FILEPATH');
		$date_time = date('Y-m-d H:i:s');
		$log_entry = array(
			'timestamp' => $date_time,
			'type' => $priority,
			'metadata' => $value
		);

		$log_entry_json = json_encode($log_entry).PHP_EOL;
		file_put_contents($log_file, $log_entry_json, FILE_APPEND);

		$out = fopen('php://stdout', 'w');
		fputs($out, "\n\r".$log_entry_json."\n\r");
		fclose($out);
	}
}


//function log($event_type, $message, $metadata = array()){
//	//Elastic log
//	$logstashHost = getenv('LOGSTASH_URL') ?? 'http://logstash:5000';
//	$client = new Client();
//	$logData = [
//		'timestamp' => date('Y-m-d H:i:s'),
//		'message' => $message,
//		'level' => 'log',
//		'event_type' => $event_type,
//		'metadata' => $metadata
//	];

//	try {
//		$response = $client->post($logstashHost, [
//			'json' => $logData
//		]);
//	} catch (Exception $e) {
//		log_event('error', 'Cannot send elastic log', ['message' => $e->getMessage()]);
//	}
//}