<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;

final class RouterFactory
{
	use Nette\StaticClass;

	public static function createRouter(Nette\Database\Connection $db): RouteList
	{
		$router = new RouteList;

		$router->addRoute('actuators/health', 'Notification:health');
		$router->addRoute('migrate', 'Notification:migrate');
		$router->addRoute('registered/<userId>', 'Notification:registered');
		$router->addRoute('notify[/<userId>]', 'Notification:notify');
		$router->addRoute('last-notification', 'Notification:lastNotification');

		return $router;
	}
}
