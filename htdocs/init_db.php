<?php

$servername = "mysql_notification";
$username = "root";
$password = "phpmyadmin";
$dbname = "SWA_notification";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// SQL query to create a table
$sql = "CREATE TABLE IF NOT EXISTS notifications (
	id SERIAL,
	user_id INT NOT NULL,
	user_email TEXT NOT NULL,
	type TEXT NOT NULL,
	data TEXT,
	send_time TIMESTAMP NOT NULL,
	PRIMARY KEY(id)
)";

// Execute the query
if ($conn->query($sql) === TRUE) {
    echo "Table created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

// Close connection
$conn->close();