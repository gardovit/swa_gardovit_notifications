composer install

sleep 10

#Register service with service register
curl -X POST -d \
"type=$SERVICE_TYPE&hostname=$HOSTNAME&port=$SERVICE_PORT&health_url=$SERVICE_HEALTH_URL"\
 http://service_register:80/services


#Init database
php init_db.php

#Start up php server as regular image
apache2-foreground