#!/bin/bash

sleep 20
echo "sleep done"

#Initialize database
run_migration(){

  echo "Database migration integration test"
  local migrate_response=$(curl -X POST http://localhost:80/migrate)
  local migrate_expected_response="\"Database initialized\""
  if [[ "$migrate_response" == *"$migrate_expected_response"* ]]; then
      echo "Migration test pass"
    else
      echo "Migration test pass fail"
      echo "Migration response was:"
      echo $migrate_response
      echo "Expected response was:"
      echo $migrate_expected_response
      exit 1
  fi
}


#Notify user about being registered
user_registered(){

  echo "Notify registered user integration test"
  local registered_response=$(curl -X POST  localhost:80/registered/1)
  local registered_expected_response="{\"message\":\"Notification send\"}"
  if [[ "$registered_response" == *"$registered_expected_response"* ]]; then
      echo "Notify registered user pass"
    else
      echo "Notify registered user fail"
      echo "Notify registered user response was:"
      echo $registered_response
      echo "Expected response was:"
      echo $registered_expected_response
      exit 1
  fi

  echo "Checking last sent notification"
  local get_last_response=$(curl -X GET  localhost:80/last-notification)
  local get_last_expected_response="{\"user_id\":1,\"user_email\":\"1\",\"type\":\"registration\",\"data\":\"{\\\"message\\\":\\\"You were registered now.\\\"}\"}"
  if [[ "$get_last_response" == *"$get_last_expected_response"* ]]; then
      echo "Check last notification pass"
    else
      echo "Check last notification fail"
      echo "Check last notification response was:"
      echo $get_last_response
      echo "Expected response was:"
      echo $get_last_expected_response
      exit 1
  fi

}





run_migration
user_registered