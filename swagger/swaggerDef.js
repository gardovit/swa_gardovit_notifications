const swaggerDefinition = {
	openapi: "3.0.0",
	info: {
		title: "Notification service",
		version: "1.0.0",
		description: "API documentatin for Notification service",
	},
	servers: [
		{
			url: "notification:80",
			description: "Docker compose address",
		},
	],
};

module.exports = swaggerDefinition;
