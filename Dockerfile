FROM php:8.1-apache
WORKDIR /var/www/html

ENV APACHE_DOCUMENT_ROOT /var/www/html/www
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

ENV LOG_FILEPATH /var/log/service_log.txt
RUN touch /var/log/service_log.txt
RUN chmod a+rw /var/log/service_log.txt

# Mod Rewrite
RUN a2enmod rewrite

# Linux Library
RUN apt-get update -y && apt-get install -y \
    libicu-dev \
    libmariadb-dev \
    unzip zip \
    zlib1g-dev \
    libpng-dev \
    libjpeg-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev

# Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# PHP Extension
RUN docker-php-ext-install gettext intl pdo_mysql gd mysqli

RUN docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd



RUN mkdir /var/www/html/temp
RUN mkdir /var/www/html/log
RUN chown -R www-data:www-data /var/www/html/temp /var/www/html/log


#Copy integratino tests
COPY resources/integration_test.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/integration_test.sh

#Startup script
COPY resources/init.sh /usr/bin/init.sh
RUN chmod +x /usr/bin/init.sh

#Install dwebsite
COPY htdocs/ /var/www/html/
RUN chown -R www-data:www-data /var/www/html/temp /var/www/html/log
RUN composer install


CMD /usr/bin/init.sh